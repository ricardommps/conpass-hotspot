import React, { Component, Fragment } from 'react';

import Context from './components/Context'
import Header from "./components/Header/Header"
import AddHotspot from './components/AddHotspot/AddHotspot'
import ListHotspot from './components/ListHotspot/ListHotspot'
import Hotspot from './components/Hotspots/Hotspots'

import store from './store/hotspots'

import { withStyles } from '@material-ui/core/styles';
import theme from './theme/theme';
import { ThemeProvider } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';

import {styles} from './app.style';

const App = (props) => {
  const hotspots = store();
  const { hotspots: markers = [] } = hotspots;
  const { Provider } = Context;
  const {classes} = props;
    return (
     <ThemeProvider theme={theme}>
        <Provider value={hotspots}>
          <Header/>
          <div className={classes.root}>
            <Grid container justify = "center" spacing={2}>
                <Grid item>
                  <AddHotspot/>
                  </Grid>
            </Grid>

            <Grid container justify = "center" spacing={10}>
                <Grid item xs={4}>
                  <ListHotspot/>
                </Grid>
            </Grid>

            { markers.map(hotspot => (
              <Hotspot
                key={hotspot.id}
                {...hotspot}
              />
            ))
            }

          </div>
        </Provider>
      </ThemeProvider>
    );
  
}
export default withStyles(styles) (App);
