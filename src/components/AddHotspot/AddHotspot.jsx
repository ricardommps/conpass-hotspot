import React, { useContext } from 'react';
import Context from '../Context';

import Fab from '@material-ui/core/Fab';

import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import {styles} from './addHotspot.style'

const AddHotspot = (props) => {
    const {classes} = props;
    const {
        isPointing,
        startPointing,
      } = useContext(Context);

      return (
        <Fab 
          variant="extended" 
          aria-label="delete" 
          color="primary" 
          className={classes.fab}
          disabled={isPointing}
          onClick={startPointing}
        >
            <AddIcon className={classes.addIcon} />
            Create Hotspot
        </Fab>
      )
}

export default withStyles(styles) (AddHotspot)