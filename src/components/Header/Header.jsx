import React, { Component, Fragment } from 'react';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Link from '@material-ui/core/Link';
import logo from '../../logo.png';
import {styles} from './header.style'

const googleUrl = 'http://www.google.com.br'
class Header extends Component {
    render() {
        const { classes } = this.props
        return (
            <div className={classes.root}>
                <AppBar position="static" className={classes.appBar}>
                    <Toolbar>
                    <img src={logo} alt="logo" className={classes.logo} />
                    <Typography className={classes.title}>

                        <Link color="primary" className={classes.link}>
                            Link fake 1
                        </Link>

                        <Link color="primary" className={classes.link}>
                            Link fake 2
                        </Link>

                        <Link color="primary" className={classes.link}>
                            Link fake 3
                        </Link>

                        <Link color="primary" className={classes.link}>
                            Link fake 4
                        </Link>

                    </Typography>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}

export default withStyles(styles) (Header)