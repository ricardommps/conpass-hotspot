export const styles = theme => ({
    root: {
        flexGrow: 1,
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
      link: {
        margin: theme.spacing(2),
      },
      logo: {
        maxWidth: 160,
      },
      appBar: {
        background: 'transparent', 
      }
})