import ReactDOM from 'react-dom';
import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';

import useResize from '../../store/useResize';
import StyledHotspot from './hostspots.style';

import Info from './Info/Info';

const Hotspot = (props) => {
    const { left, top, windowSize, title, text, id} = props
    const { height: currentHeight, width: currentWidth } = useResize();
    const [isShowingInfo, setIsShowingInfo] = useState(!title && !text);

    const toggleContent = () => {
        setIsShowingInfo(!isShowingInfo);
    };

    const currenPositionTop = (currentHeight * top) / windowSize.height;
    const currenPositionLeft = (currentWidth * left) / windowSize.width;
    return ReactDOM.createPortal(
        <Fragment>
          <StyledHotspot
            onClick={toggleContent}
            left={currenPositionLeft}
            top={currenPositionTop}
          />
          { isShowingInfo && (
            <Info
              id={id}
              title={title}
              text={text}
              left={currenPositionLeft}
              top={currenPositionTop}
              open={isShowingInfo}
            />
          )
          }
        </Fragment>,
        document.body,
      );

}

Hotspot.propTypes = {
    id: PropTypes.string.isRequired,
    left: PropTypes.number,
    title: PropTypes.string,
    text: PropTypes.string,
    top: PropTypes.number,
    windowSize: PropTypes.shape({
      width: PropTypes.number.isRequired,
      height: PropTypes.number.isRequired,
    }),
  };
  
  Hotspot.defaultProps = {
    left: 0,
    text: '',
    top: 0,
    title: '',
  };
  
  export default Hotspot;