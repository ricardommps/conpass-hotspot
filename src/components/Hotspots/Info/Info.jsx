import React, { useContext, useState, Fragment } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import Context from '../../Context';
import {styles} from './info.style'

const Info = (props) => {
    const {left, top, id, title, text, open, classes} = props
    const { updateHotspot } = useContext(Context);
    const [updatedTitle, setTitle] = useState(title);
    const [updatedText, setText] = useState(text);
    const [anchorEl, setAnchorEl] = React.useState(open);
    const openPopover = Boolean(anchorEl);
    const idPopover = openPopover ? 'info-popover' : undefined;
    const disableForm = title && text ? true : false
    
    function handleClose() {
        setAnchorEl(null);
    }

    function handleSave () {
        setAnchorEl(null);
        updateHotspot({
            id,
            text: updatedText,
            title: updatedTitle,
        }) 
    }

    return (
        <Fragment>
            <Popover
                id={idPopover} 
                open={openPopover}
                onClose={handleClose}
                anchorReference="anchorPosition"
                anchorPosition={{ top: top, left: left }}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <div>
                    <Grid container spacing={2}>
                        <Grid item>
                            <TextField
                                id="standard-title"
                                label="Title"
                                className={classes.textField}
                                value={updatedTitle}
                                onChange={({ target: { value } }) => setTitle(value)}
                                margin="normal"
                                variant="outlined"
                                disabled={disableForm}
                             />
                        </Grid>
                    </Grid>

                    <Grid container spacing={2}>
                        <Grid item>
                            <TextField
                                id="standard-title"
                                label="Title"
                                multiline
                                rows="4"
                                className={classes.textField}
                                value={updatedText}
                                onChange={({ target: { value } }) => setText(value)}
                                margin="normal"
                                variant="outlined"
                                disabled={disableForm}
                            />
                        </Grid>
                    </Grid>

                    <Grid container spacing={2}>
                        <Grid item >
                            <Button 
                                variant="contained" 
                                className={classes.button} 
                                color="primary" 
                                onClick={() => handleSave()}
                                disabled={disableForm}
                            >
                                Salve
                            </Button>    
                        </Grid>
                    </Grid>
                </div>
             </Popover>
        </Fragment>
    )
}

Info.propTypes = {
    id: PropTypes.string.isRequired,
    left: PropTypes.number,
    text: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    top: PropTypes.number,
    open: PropTypes.bool
  };
  
  Info.defaultProps = {
    left: 0,
    top: 0,
    open: null
  };

export default withStyles(styles) (Info)