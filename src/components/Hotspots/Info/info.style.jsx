export const styles = theme => ({
    popover: {
        pointerEvents: 'none',
      },
      button: {
        margin: theme.spacing(1),
        color: '#fff'
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
      },
      container: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      margin: {
        margin: theme.spacing(1),
      },
})