import React, { useContext, Fragment } from 'react';
import Context from '../Context';

import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';

import {styles} from './listHotspot.style'

const ListHotspot = (props) => {
    const { classes } = props
    const {
        hotspots,
        removeHotspot,
    } = useContext(Context);
    return (
        <Fragment>
            <Typography variant="h6" className={classes.title}>
                List of Hotspots
            </Typography>
            <div className={classes.demo}>
                {hotspots.length == 0 ?
                    <div><h1>Lista vazia</h1></div>
                    :
                    <List>
                        {hotspots.map((item,index) => (
                            <ListItem key={index}>
                                <ListItemText
                                    primary={`Hotspot #${index + 1}`}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" aria-label="delete" onClick={() => removeHotspot(index)}>
                                        <DeleteIcon />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        ))}
                        
                    </List>
                }
           
          </div>
        </Fragment>
    )
}

export default withStyles(styles) (ListHotspot)