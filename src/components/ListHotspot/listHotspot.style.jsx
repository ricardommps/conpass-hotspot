export const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
      },
      demo: {
        backgroundColor: theme.palette.background.paper,
      },
      title: {
        margin: theme.spacing(4, 0, 2),
        background: '#ebebeb',
        padding: theme.spacing(1)
      },
})